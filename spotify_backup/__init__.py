#!/usr/bin/env python3

import appdirs
import argparse
import configparser
import json
import os.path
import random
import re
import requests
import string
import sys
import webbrowser

CONFIG_KEYS = [
    "client_id",
    "client_secret",
    "redirect_uri",
    "token_file"
]

CONFIG_FILE = None
CONFIG = None

def parse_options():
    parser = argparse.ArgumentParser()

    parser.add_argument("out", help="Output JSON file containing the tracks")
    parser.add_argument("--config", "-c", dest="config",
                        help="Configuration file", default="/etc/spotify-backup.yml")

    return parser.parse_args()

def parse_config(file: str) -> configparser.ConfigParser:
    errors = []
    changed = False

    config = configparser.ConfigParser()

    if os.path.exists(file):
        config.read(file)

    if "spotify" not in config.sections():
        config["spotify"] = {}

    if "local" not in config.sections():
        config["local"] = {}

    if "redirect_uri" not in config["spotify"]:
        config["spotify"]["redirect_uri"] = "http://localhost:64207"
        changed = True
    if "client_id" not in config["spotify"] or not config["spotify"]["client_id"]:
        config["spotify"]["client_id"] = ""
        errors.append("client_id not set")
    if "client_secret" not in config["spotify"] or not config["spotify"]["client_secret"]:
        config["spotify"]["client_secret"] = ""
        errors.append("client_secret not set")
    if "access_token" not in config["local"]:
        config["local"]["access_token"] = ""
        changed = True
    if "refresh_token" not in config["local"]:
        config["local"]["refresh_token"] = ""
        changed = True

    if changed:
        with open(file, "w") as f:
            config.write(f)
    if errors:
        print("Config file errors:\n{}".format("\n".join(errors), file=sys.stderr))
        sys.exit(1)

    return config

def get_token(data: dict):
    global CONFIG_FILE
    global CONFIG

    r = requests.post("https://accounts.spotify.com/api/token", data=data, auth=(CONFIG["spotify"]["client_id"], CONFIG["spotify"]["client_secret"]))
    if r.status_code != 200:
        raise RuntimeError("Could not get token with code: status {}: {}".format(r.status_code, r.text))
    j = r.json()

    CONFIG["local"]["access_token"] = j["access_token"]
    CONFIG["local"]["refresh_token"] = j["refresh_token"]

    with open(CONFIG_FILE, "w") as f:
        CONFIG.write(f)

def get(url: str, recursive=False) -> dict:
    global CONFIG

    r = requests.get("https://api.spotify.com/v1{}".format(url), headers={"Authorization": "Bearer {}".format(CONFIG["local"]["access_token"])})
    if r.status_code == 401:
        if recursive:
            raise RuntimeError("Previous token refresh didn't work.")

        print("Refreshing token")

        get_token({"grant_type": "refresh_token", "refresh_token": CONFIG["local"]["refresh_token"]})

        return get(url, recursive=True)

    j = r.json()

    if "error" in j:
        raise RuntimeError("Error during request to {}: {}".format(url, j))

    return j

def main():
    global CONFIG_FILE
    global CONFIG

    args = parse_options()

    CONFIG_FILE = args.config
    CONFIG = parse_config(CONFIG_FILE)

    state = "".join(random.choices(string.ascii_uppercase + string.digits, k = 32))

    if not CONFIG["local"]["access_token"] or not CONFIG["local"]["refresh_token"]:
        print("Requesting token...")

        url = "https://accounts.spotify.com/authorize?client_id={client_id}&response_type=code&redirect_uri={redirect_uri}&state={state}&scope=user-library-read%20user-top-read%20user-read-recently-played".format(
            client_id=CONFIG["spotify"]["client_id"],
            redirect_uri=CONFIG["spotify"]["redirect_uri"],
            state=state
        )

        print("Opening {}".format(url))
        webbrowser.open(url)

        print("state: {}".format(state))
        print("Code (copy from URL): ")
        code = sys.stdin.readline().strip()
        p = re.compile(r".*\?code=([^&\?]*)")
        m = p.match(code)
        if m:
            code = m.group(1)

        print(code)

        get_token({"grant_type": "authorization_code", "code": code, "redirect_uri": CONFIG["spotify"]["redirect_uri"]})

    offset = 0
    songs = []
    nextpattern = re.compile(r".*?offset=([^&\?]*)")
    while True:
        j = get("/me/tracks?offset={}&limit=50".format(offset))
        songs.extend(j["items"])

        if j["next"]:
            offset = int(nextpattern.match(j["next"]).group(1))
        else:
            break

    n_chunks = int(len(songs) / 100) + (1 if len(songs) % 100 > 0 else 0)
    chunks = []
    for i in range(n_chunks):
        try:
            chunks.append(songs[i*100:(i+1)*100])
        except IndexError:
            chunks.append(songs[i*100:])

    songs = []
    for chunk in chunks:
        print("Getting chunk info...")
        ids = [track["track"]["id"] for track in chunk]
        features = get("/audio-features?ids={}".format(",".join(ids)))

        for i in range(len(chunk)):
            track = chunk[i]
            track["features"] = features["audio_features"][i]
            songs.append(track)
        print(len(chunk))
        print("Got {} songs".format(len(songs)))

    with open(args.out, "w") as f:
        json.dump(songs, f)


if __name__ == "__main__":
    main()

class InvalidConfig(Exception):
    def __init__(self, reason):
        self.reason = reason
