#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name="spotify-backup",
    version="1.0.0",
    description="Spotify Backup",
    python_requires='>=3.6',

    entry_points=dict(
        console_scripts=[
            "spotify-backup = spotify_backup:main"
        ]
    ),

    author="Nathan Monfils",
    author_email="nathan.monfils@hotmail.fr",

    packages=["spotify_backup"],

    install_requires=[
        "requests>=2.0.0"
    ]
)
